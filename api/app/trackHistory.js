const express = require('express');
const auth = require('../middleware/auth');
const User = require('../models/Users');
const TrackHistory = require('../models/TrackHistory');

const router  = express.Router();

router.post('/', auth, async (req, res) => {
    const dateTime = new Date().toISOString();

    const  trackId = ({user: req.user._id, track: req.body.track, dateTime});

    const historyTrack = new TrackHistory(trackId);

    await historyTrack.save();
    res.send(historyTrack);
});

router.get('/', auth, async (req, res) => {
     await TrackHistory.find({user: req.user._id}).populate({path: 'track', populate: {path: 'album', populate: {path: 'artist'}}})
        .then(track => {
            console.log(track);
            res.send(track)
        })
        .catch(() => res.sendStatus(400));
});

module.exports = router;