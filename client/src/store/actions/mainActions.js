import axios from "../../axiosBase"

export const ARTISTS_REQUEST = "ARTISTS_REQUEST";
export const ARTISTS_REQUEST_SUCCESS = "ARTISTS_REQUEST_SUCCESS";
export const ALBUM_REQUEST = "ALBUM_REQUEST";
export const ALBUM_REQUEST_SUCCESS = "ALBUM_REQUEST_SUCCESS";
export const TRACK_REQUEST = "TRACK_REQUEST";
export const TRACK_REQUEST_SUCCESS = "TRACK_REQUEST_SUCCESS";
export const TRACK_HISTORY_SUCCESS = "TRACK_HISTORY_SUCCESS";
export const TRACK_HISTORY_REQUEST = "TRACK_HISTORY_REQUEST";



export const artistsRequest = () => {
    return {type: ARTISTS_REQUEST}
};

export const artistRequestSuccess = artists => {
    return {type: ARTISTS_REQUEST_SUCCESS, artists}
};

export const albumsRequest = () => {
    return {type: ALBUM_REQUEST}
};

export const albumRequestSuccess = albums => {
    return {type: ALBUM_REQUEST_SUCCESS, albums}
};

export const trackRequest = () => {
    return {type: TRACK_REQUEST}
};

export const trackRequestSuccess = tracks => {
    return {type: TRACK_REQUEST_SUCCESS, tracks}
};

export const trackHistorySuccess = tracks => {
    return {type: TRACK_HISTORY_SUCCESS, tracks}
};

export const trackHistoryRequest = (tracks) => {
    return {type: TRACK_HISTORY_REQUEST, tracks}
};

export const fetchGetArtists = () => {
    return (dispatch) => {
        dispatch(artistsRequest());
        axios.get('/artist').then(response => {
            dispatch(artistRequestSuccess(response.data));
        })
    }
};

export const fetchGetAlbum = id => {
    return (dispatch) => {
        dispatch(albumsRequest());
        axios.get('/album?artist=' + id).then(response => {
            dispatch(albumRequestSuccess(response.data))
        })
    }
};

export const fetchGetTracks = id => {
    return (dispatch) => {
        axios.get('/track?album=' + id).then(response => {
            dispatch(trackRequestSuccess(response.data))
        })
    }
};

export const addTrackToHistory = track => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        axios.post('/track_history', track, {headers: {'Authorization': token}}).then(() => {
            dispatch(trackHistorySuccess());
        })
    }
};

export const fetchGetTrackHistory = () => {
    return (dispatch, getState) => {
        const token = getState().users.user.token;
        axios.get('/track_history', {headers: {'Authorization': token}}).then(response => {
            dispatch(trackHistoryRequest(response.data))
        })
    }
};
