import React, {Component, Fragment} from 'react';
import './App.css';
import {Route, Switch, withRouter} from "react-router-dom";
import Artists from "../components/Artists/Artists";
import Albums from "../components/Albums/Albums";
import Tracks from "../components/Tracks/Tracks";
import Toolbar from "../components/UI/Toolbar/Toolbar";
import {Container} from "reactstrap";
import {connect} from "react-redux";
import Register from "./Register/Register";
import Login from "./Login/Login";
import TrackHistory from "../components/TrackHistory/TrackHistory";

class App extends Component {
  render() {
    return (
      <Fragment>
          <header>
              <Toolbar user={this.props.user}/>
          </header>
          <Container style={{marginTop: '20px'}}>
              <Switch>
                  <Route path="/" exact component={Artists}/>
                  <Route path="/register" exact component={Register}/>
                  <Route path="/login" exact component={Login}/>
                  <Route path="/album/:id/:artist" exact component={Albums}/>
                  <Route path='/track/:id/:artist/:album' exact component={Tracks}/>
                  <Route path='/track_history' exact component={TrackHistory}/>
              </Switch>
          </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user
});

export default withRouter(connect(mapStateToProps)(App));
