import React from 'react';
import {CardImg} from "reactstrap";


const ImageThumbnail = props => {
    if (props.image !== "null") {
        let image = 'http://localhost:8000/uploads/artist/' + props.image;
        return <CardImg src={image} className="img-thumbnail" alt="News Image" />;
    }
    return null;
};

export default ImageThumbnail;