import React, {Component} from 'react';
import {fetchGetAlbum} from "../../store/actions/mainActions";
import {connect} from "react-redux";
import {Card, CardBody, CardText, CardTitle} from "reactstrap";
import AlbumThumbnail from "../AlbumThumbnail/AlbumThumbnail";
import {NavLink} from "react-router-dom";

import './Albums.css';

class Albums extends Component {
    componentDidMount() {
        this.props.loadAlbum(this.props.match.params.id);
    }

    render() {
        let album = this.props.albums.map(id => {
            return (
                <Card key={id._id} className="album-card">
                    <CardBody>
                        <CardTitle className="album-name">{this.props.match.params.artist + '/' + id.name}</CardTitle>
                    </CardBody>
                    <AlbumThumbnail image={id.photo}/>
                    <CardBody>
                        <CardText>{id.info}</CardText>
                        <CardText>{id.date}</CardText>
                        <NavLink to={`/track/${id._id}/${this.props.match.params.artist}/${id.name}`}>Tracks</NavLink>
                    </CardBody>
                </Card>
            )
        });
        return (
            <div>
                <NavLink to="/">Artist list</NavLink>
                <h4>Album list</h4>
                <div>
                    {album}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albumsArtists.albums,
});

const mapDispatchToProps = dispatch => ({
    loadAlbum: id => dispatch(fetchGetAlbum(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Albums);
